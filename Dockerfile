FROM python:2.7

MAINTAINER i.phongsakornp@gmail.com

## Install openssh-server
RUN apt-get update && apt-get install -y openssh-server supervisor
RUN mkdir -p /var/run/sshd /var/log/supervisor

## Change root password
RUN echo 'root:abc' | chpasswd

## Edit sshd_config
RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN echo '' >> /etc/ssh/sshd_config

## Add this line to fix bug with Java SSH.
RUN echo 'KexAlgorithms ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha1,diffie-hellman-group-exchange-sha256,diffie-hellman-group14-sha1' >> /etc/ssh/sshd_config

## Application
ADD . /app
WORKDIR /app
RUN pip install -r requirements.txt

## Supervisor
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

## Run SSH Server as default.
## If use FIG, these EXPOSE and CMD may be overrided by 'ports' and 'command' in FIG.
EXPOSE 22 8080

CMD /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
