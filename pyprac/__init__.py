from flask import Flask
from api import book_api_blueprint
from dataaccess import current_data_access
from configs import *

def create_app(database=None, use_instance_folder=False):
    """Return pyprac application instance.
    """
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object('pyprac.configs')

    # Override default application config by putting application.cfg file in default flask instance folder.
    # For example in case of deployment specific configuration.
    # http://flask.pocoo.org/docs/0.10/config/#instance-folders
    if use_instance_folder:
        app.config.from_pyfile('application.cfg', silent=True)

    current_data_access.database = database

    app.register_blueprint(book_api_blueprint)

    return app
