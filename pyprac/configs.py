"""
    pyprac default settings
"""

CONFIG_NAME = 'Default'


# Basic configuration
APP_HOST = '127.0.0.1'  # localhost
DEBUG = True
SECRET_KEY = '007'


# Databases
DB_NAME = 'pyprac'
DB_HOST = '127.0.0.1'
DB_PORT = 27017