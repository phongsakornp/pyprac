from ...dataaccess import current_data_access


def create_book(title, author):
    book = {
        'title': title,
        'author': author
    }
    current_data_access.database.books.insert(book)
    return book