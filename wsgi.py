#!flask/bin/python
import pymongo
from pyprac import create_app
from pymongo import MongoClient


database = None
app = None

# Create a dummy application to access the configs.
dummy_app = create_app(use_instance_folder=True)
config = dummy_app.config

try:
    client = MongoClient(host=config['DB_HOST'], port=config['DB_PORT'])
    database = client[config['DB_NAME']]
except pymongo.errors.ConnectionFailure as e:
    print "! WTF !-- Oops!! Database Connection Failure :("
    print "! WTF !-- Will run app without database"   

if database:
    app = create_app(database, use_instance_folder=True)
else:
    app = create_app(use_instance_folder=True)
    
if __name__ == '__main__':
    app.run(host=config['APP_HOST'], debug=config['DEBUG'])