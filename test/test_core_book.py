from bson.objectid import ObjectId

book_title = 'MyBook'
book_author = 'AlbertE.'


def create_book(testing_app):
    from pyprac.core.book import models

    book = models.create_book(book_title, book_author)
    return book


def test_create_book(testing_app):
    book = create_book(testing_app)
    assert book['title'] == book_title
    assert book['author'] == book_author


def test_create_book_objectid(testing_app):
    book = create_book(testing_app)
    assert isinstance(book['_id'], ObjectId)


def test_create_book_and_get(testing_app):
    from pyprac.core.book import models

    book = create_book(testing_app)
    new_book = models.current_data_access.database.books.find_one({'_id': book['_id']})
    assert new_book
    assert new_book['title'] == book_title