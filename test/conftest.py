import pytest
import os
import mongomock


@pytest.fixture
def pyprac_application(monkeypatch):
    monkeypatch.syspath_prepend(
        os.path.abspath(os.path.join(
            os.path.dirname(__file__), '../'
        ))
    )
    import pyprac
    return pyprac


@pytest.fixture
def default_app(pyprac_application):
    app = pyprac_application.create_app()
    return app


@pytest.fixture
def testing_app(pyprac_application):
    client = mongomock.MongoClient()
    database = client['test']
    app = pyprac_application.create_app(database)
    app.config['TESTING'] = True
    return app